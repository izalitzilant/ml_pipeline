import pickle
from typing import Dict

import numpy as np
import pandas as pd
from ray import serve
from fastapi import FastAPI, UploadFile
from dotenv import load_dotenv
from sklearn.model_selection import train_test_split


load_dotenv("./.env")


app = FastAPI()


@serve.deployment(route_prefix="/stacking_predictor")
@serve.ingress(app)
class StackingPredictService:

    @app.get("/")
    async def root(self) -> str:
        return "RootEndpoint"
    
    # @app.get("/predict")
    # async def predict(self, rooms: int,
    #                   bathrooms: int, landsize: int,
    #                   lattitude: int, longtitude: int) -> int:
    #     with open(f"./models/model.pkl", "rb") as model_file:   #TODO change hardcode string to config reading (somehow)
    #         model = pickle.load(model_file)

    #     params = np.array([rooms, bathrooms, landsize, lattitude, longtitude]).reshape(1, -1)

    #     prediction = model.predict(params)[0]
    #     return prediction

    @app.post("/upload_dataset")
    async def upload_dataset(self, file: UploadFile) -> dict:
        data = pd.read_csv(file.file)

        with open(f"./models/meta_model/model.pkl", "rb") as model_file:   #TODO change hardcode string to config reading (somehow)
            model = pickle.load(model_file)

        pred_y = model.predict(data[["log", "knn"]])

        return {"filename": file.filename,
                "pred_y": pred_y}
