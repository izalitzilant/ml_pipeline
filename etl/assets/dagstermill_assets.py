from dagstermill import define_dagstermill_asset
from dagster import file_relative_path, AssetIn

from .default_assets import fit_evaluate_asset


model_check = define_dagstermill_asset(
    name="model_check",
    notebook_path=file_relative_path(__file__, "../notebooks/model_check.ipynb"),
    ins={"fit_evaluate_asset": AssetIn(key=["fit_evaluate_asset"])}
)
