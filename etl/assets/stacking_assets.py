import base64
import pickle
import seaborn
import pandas as pd
from io import BytesIO
from matplotlib import pyplot as plt
from pandas import DataFrame
from dagster import AssetIn, MetadataValue, Output, asset
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error
from ray import serve

from etl.configs import EtlConfig
from stacking_web_service.main import StackingPredictService


def make_plot(eval_metric):
    plt.clf()
    training_plot = seaborn.lineplot(eval_metric)
    fig = training_plot.get_figure()
    buffer = BytesIO()
    fig.savefig(buffer)
    image_data = base64.b64encode(buffer.getvalue())
    return MetadataValue.md(f"![img](data:image/png;base64,{image_data.decode()})")


@asset(group_name="stacking")
def get_data(config: EtlConfig) -> DataFrame:
    data = pd.read_csv(f"{config.datasets_path}/melb_data.csv")
    return data


@asset(group_name="stacking", ins={"data": AssetIn(key=["get_data"])})
def prep_data(data: DataFrame):
    features = ["Rooms", "Bathroom", "Landsize", "Lattitude", "Longtitude"]                         # get from the config file
    df = data[features]
    t_df = data.Price
    return df, t_df


@asset(group_name="stacking", ins={"data": AssetIn(key=["prep_data"])})
def train_test_asset(data: tuple):
    df, t_df = data
    # Разделите данные на трейн и тест
    X_df, X_test, y_df, y_test = train_test_split(df, t_df, random_state=322, test_size=0.2)
    return X_df, X_test, y_df, y_test


@asset(group_name="stacking", ins={"data": AssetIn(key=["train_test_asset"])})
def blending_asset(data) -> Output:
    X_df, X_test, y_df, y_test = data
    # Разделите трейн выборку на 2 части для блендинга
    X_train, X_valid, y_train, y_valid = train_test_split(X_df, y_df, random_state=322, test_size=0.4)
    return X_train, X_valid, y_train, y_valid


@asset(group_name="stacking", ins={"get_data": AssetIn(key=["blending_asset"])})
def log_model(get_data) -> Output:
    X_train, X_valid, y_train, y_valid = get_data
    logistic_regression = LogisticRegression()
    logistic_regression.fit(X_train, y_train)
    pred_y = logistic_regression.predict(X_valid)
    scores = {
        "mAe": mean_absolute_error(y_valid, pred_y),
        "r2s": r2_score(y_valid, pred_y),
        "mse": mean_squared_error(y_valid, pred_y)
    }

    metadata = {}
    metadata["scores"] = scores

    return Output(logistic_regression, metadata={"mAe": mean_absolute_error(y_valid, pred_y), "r2s": r2_score(y_valid, pred_y), "mse": mean_squared_error(y_valid, pred_y)})



@asset(group_name="stacking", ins={"get_data": AssetIn(key=["blending_asset"]), "prep_data": AssetIn(key=["prep_data"])})
def knn_model(get_data, prep_data):
    df, t_df = prep_data
    X_train, X_valid, y_train, y_valid = get_data
    knn = KNeighborsRegressor()
    knn.fit(X_train, y_train)
    pred_y = knn.predict(X_valid)
    scores = {
        "mAe": mean_absolute_error(y_valid, pred_y),
        "r2s": r2_score(y_valid, pred_y),
        "mse": mean_squared_error(y_valid, pred_y)
    }

    metadata = {}
    metadata["scores"] = scores

    return Output(knn, metadata={"mAe": mean_absolute_error(y_valid, pred_y), "r2s": r2_score(y_valid, pred_y), "mse": mean_squared_error(y_valid, pred_y)})


@asset(group_name="stacking")
def meta_features_dataset(knn_model: KNeighborsRegressor, log_model: LogisticRegression, blending_asset, train_test_asset, prep_data) -> DataFrame:
    X_train, X_valid, y_train, y_valid = blending_asset
    X_df, X_test, y_df, y_test = train_test_asset

    meta_df = pd.DataFrame(data=0, index=range(len(X_test)), columns=['log', 'knn'])
    meta_df['log'] = log_model.predict(X_test)
    meta_df['knn'] = knn_model.predict(X_test)
    meta_df.to_csv("./datasets/metadf/meta_df.csv")

    return meta_df


@asset(group_name="stacking")
def meta_alg_fit(meta_features_dataset: DataFrame, blending_asset, train_test_asset):
    X_train, X_valid, y_train, y_valid = blending_asset
    X_df, X_test, y_df, y_test = train_test_asset

    meta_df  = meta_features_dataset
    log_reg = LogisticRegression()
    log_reg.fit(meta_df, y_test)

    return log_reg


@asset(group_name="stacking")
def serve_alg(config: EtlConfig, meta_alg_fit: LogisticRegression):
    model = meta_alg_fit
    with open(f"{config.models_path}/meta_model/model.pkl", "wb") as model_file:
        pickle.dump(model, model_file)
    serve.run(StackingPredictService.bind())

