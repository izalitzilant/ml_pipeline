import pickle

import pandas as pd
from ray import serve
from pandas import DataFrame
from dagster import AssetIn, asset
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_absolute_error, r2_score, mean_squared_error
from sklearn.model_selection import train_test_split

from ..configs import EtlConfig
from web_service.main import PredictService


@asset(group_name="default")
def get_data_asset(config: EtlConfig) -> DataFrame:
    data = pd.read_csv(f"{config.datasets_path}/melb_data.csv")
    return data


@asset(group_name="default", ins={"get_data": AssetIn(key=["get_data_asset"])})  # change to read from the config
def preprocess_data_asset(get_data: DataFrame) -> DataFrame:
    data = get_data
    
    return data


@asset(group_name="default", ins={"get_data": AssetIn(key=["preprocess_data_asset"])})  # change to read from the config
def fit_evaluate_asset(get_data: DataFrame) -> DecisionTreeRegressor:
    data = get_data

    features = ["Rooms", "Bathroom", "Landsize", "Lattitude", "Longtitude"]  # get from the config file
    X = data[features]
    y = data.Price
    train_X, val_X, train_y, val_y = train_test_split(X, y, random_state=1)

    model = DecisionTreeRegressor(random_state=1)  # use dagster config to configurate choosing model
    model.fit(train_X, train_y)

    pred_y = model.predict(val_X)
    scores = {
        "mAe": mean_absolute_error(val_y, pred_y),
        "r2s": r2_score(val_y, pred_y),
        "mse": mean_squared_error(val_y, pred_y)
    }

    return model


@asset(group_name="default", ins={"get_model": AssetIn(["fit_evaluate_asset"])})
def serve_asset(get_model: DecisionTreeRegressor, config: EtlConfig) -> None:
    model = get_model
    with open(f"{config.models_path}/model.pkl", "wb") as model_file:
        pickle.dump(model, model_file)
    serve.run(PredictService.bind())
    