import pandas as pd
from dagster import DynamicOut, DynamicOutput, op
from pandas import DataFrame

from etl.configs import EtlConfig


@op
def get_data(config: EtlConfig) -> DataFrame:
    data = pd.read_csv(config.datasets_path + "/" + config.dataset_filename)
    return data

@op(out=DynamicOut())
def get_xgboost_data(config: EtlConfig) -> DataFrame:
    data = pd.read_csv(config.datasets_path + "/" + config.dataset_filename)
    for i in range(config.xgboost_start, config.xgboost_end, config.xgboost_step):
        yield DynamicOutput(i, data)
