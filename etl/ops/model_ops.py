import copy
import pandas as pd
from dagster import op
from pandas import DataFrame
from dotenv import load_dotenv
from xgboost import XGBRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error, r2_score, mean_squared_error

from etl.configs import EtlConfig


load_dotenv("../.env")


@op
def eval_model_op(val_y, pred_y) -> dict:
    scores = {
        "mAe": mean_absolute_error(val_y, pred_y),
        "r2s": r2_score(val_y, pred_y),
        "mse": mean_squared_error(val_y, pred_y)
    }
    return scores


@op
def model_fit_op(model, train_X, train_y):
    model.fit(train_X, train_y)
    return model

@op
def get_decsision_tree_model(config: EtlConfig, get_data: DataFrame) -> tuple:
    data = get_data

    features = ["Rooms", "Bathroom", "Landsize", "Lattitude", "Longtitude"]  # get from the config file
    X = data[features]
    y = data.Price
    train_X, val_X, train_y, val_y = train_test_split(X, y, random_state=1)

    model = DecisionTreeRegressor(random_state=1)  # use dagster config to configurate choosing model
    model = model_fit_op(model, train_X, train_y)

    pred_y = model.predict(val_X)
    scores = eval_model_op(val_y, pred_y)
    
    return model, scores

@op
def get_xgboost_model(data: DataFrame) -> tuple:
    n_est = data[1]
    data = data[0]
    features = ["Rooms", "Distance", "Landsize", "BuildingArea", "YearBuilt"]
    X = data[features]
    y = data.Price

    X_train, X_valid, y_train, y_valid = train_test_split(X, y)

    model = XGBRegressor(n_estimators=n_est)
    model = model_fit_op(model, X_train, y_train)

    pred_y = model.predict(X_valid)
    scores = eval_model_op(y_valid, pred_y)
    
    return model, scores


@op
def model_handling(models: list):
    for model in models:
        print(model)


@op
def xgboost_handling(config: EtlConfig, data: DataFrame) -> list:
    models = []
    for i in range(config.xgboost_start, config.xgboost_end, config.xgboost_step):
        models.append(get_xgboost_model(data, i))
    return models


@op
def get_n_est(config: EtlConfig) -> list:
    n_est = 1
    
