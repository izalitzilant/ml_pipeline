import os

from dagster import Config
from dotenv import load_dotenv


load_dotenv("../.env")


class EtlConfig(Config):
    models_path: str = os.getenv("MODELS_PATH")
    datasets_path: str = os.getenv("DATASETS_PATH")
    n_models: str = os.getenv("N_MODELS")
    dataset_filename: str = os.getenv("DATASET_FILENAME")
    xgboost_datasets_path: str = os.getenv("XGBOOST_DATASETS_PATH")
    xgboost_start: int = int(os.getenv('XGBOOST_START_N_ESTIMATORS'))
    xgboost_end: int = int(os.getenv('XGBOOST_FINISH_N_ESTIMATORS'))
    xgboost_step: int = int(os.getenv('XGBOOST_STEP'))
    
