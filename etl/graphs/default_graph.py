import os

from dagster import graph
from dotenv import load_dotenv

from etl.ops.data import get_data
from etl.ops.model_ops import get_decsision_tree_model, model_handling


load_dotenv("../.env")


@graph
def models_configuration():
    data = get_data()
    models = []
    print(int(os.getenv('N_MODELS')))
    for i in range(int(os.getenv('N_MODELS'))):
        models.append(get_decsision_tree_model(data))
    model_handling(models)
