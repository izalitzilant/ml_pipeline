import os

import pandas as pd
from pandas import DataFrame
from dagster import graph, op
from dotenv import load_dotenv
from sklearn.model_selection import train_test_split

from etl.configs import EtlConfig
from etl.ops.data import get_data, get_xgboost_data
from etl.ops.model_ops import get_decsision_tree_model, get_n_est, get_xgboost_model, model_handling, xgboost_handling


load_dotenv("../.env")


# @graph
# def xgboost_models_configuration():
#     # data_and_list = map(get_xgboost_data())

#     models = []
#     xgboost_start = int(os.getenv('XGBOOST_START_N_ESTIMATORS'))
#     xgboost_end = int(os.getenv('XGBOOST_FINISH_N_ESTIMATORS'))
#     step = int(os.getenv('XGBOOST_STEP'))
#     # models = xgboost_handling(data)
#     for i in data_and_list.collect():
#         models.append(get_xgboost_model(i))
#     model_handling(models)
#     # dagster_logger.error(f"{xgboost_end}, {xgboost_start}, {step}")

    