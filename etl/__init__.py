from dagster import Definitions, load_assets_from_modules, load_assets_from_package_module, repository, with_resources
from dagstermill import local_output_notebook_io_manager

from etl.assets import stacking_assets
from .graphs import default_graph, non_default_graph
from .assets import default_assets, dagstermill_assets, another_rep
from . import ops

@repository
def default():
    return with_resources(
            load_assets_from_modules([default_assets, dagstermill_assets]),
            resource_defs={
                "output_notebook_io_manager": local_output_notebook_io_manager,
            },
        )


@repository
def another_repository():
    return [default_graph.models_configuration.to_job(),]
            # non_default_graph.xgboost_models_configuration.to_job()]


@repository
def another_rep():
    return [load_assets_from_modules([another_rep])]

@repository
def another_rep():
    return [load_assets_from_modules([stacking_assets])]
